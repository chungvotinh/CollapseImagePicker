//
//  ShowSavedImageViewController.h
//  PhotosPicker
//
//  Created by Chung BD on 7/28/15.
//  Copyright (c) 2015 Delightful. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowSavedImageViewController : UIViewController
//@property (weak, nonatomic) IBOutlet UIImageView *imgShow;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) UIImage *img;
@end
