//
//  UIView+RenderViewToImage.h
//  PhotosPicker
//
//  Created by Chung BD on 7/28/15.
//  Copyright (c) 2015 Delightful. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RenderViewToImage)
- (UIImage *)imageByRenderingView;
@end
