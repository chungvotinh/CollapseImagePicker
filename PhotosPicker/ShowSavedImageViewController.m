//
//  ShowSavedImageViewController.m
//  PhotosPicker
//
//  Created by Chung BD on 7/28/15.
//  Copyright (c) 2015 Delightful. All rights reserved.
//

#import "ShowSavedImageViewController.h"

@implementation ShowSavedImageViewController

#pragma mark - Life Cycle
- (void)viewDidLoad {

}
- (void)viewDidAppear:(BOOL)animated {
    UIImageView *imgView = [[UIImageView alloc] initWithImage:self.img];
    //            imgView.frame = CGRectMake(0, 0, scrollView.frame.size.width, scrollView.frame.size.height);
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.scrollView addSubview:imgView];
    [self.scrollView setContentSize:imgView.frame.size];
    [self.scrollView setContentInset:UIEdgeInsetsMake(1, 1, 1, 1)];
    
//    UIScrollView *superView = (UIScrollView *)[self.imgShow superview];
//    [self.imgShow removeFromSuperview];
//    self.imgShow.frame = CGRectMake(0, 0, superView.frame.size.width, superView.frame.size.height);
//    [superView addSubview:self.imgShow];

}

@end
