//
//  UIView+RenderViewToImage.m
//  PhotosPicker
//
//  Created by Chung BD on 7/28/15.
//  Copyright (c) 2015 Delightful. All rights reserved.
//

#import "UIView+RenderViewToImage.h"

@implementation UIView (RenderViewToImage)
- (UIImage *)imageByRenderingView
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:YES];
    UIImage * snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}
@end
