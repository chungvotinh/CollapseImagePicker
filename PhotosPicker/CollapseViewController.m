//
//  CollapseViewController.m
//  PhotosPicker
//
//  Created by Chung BD on 7/28/15.
//  Copyright (c) 2015 Delightful. All rights reserved.
//

#import "CollapseViewController.h"
#import "ShowSavedImageViewController.h"
#import "UIView+RenderViewToImage.h"

@interface CollapseViewController ()
@property (weak, nonatomic) IBOutlet UIView *collapseView;
@property (weak, nonatomic) IBOutlet UIImageView *img;

@end

@implementation CollapseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Function
- (void) initView {
    [self addImageToScrollViews];
    [self.view bringSubviewToFront:_img];
}

- (void) addImageToScrollViews {
    NSMutableArray *arraysubView = [NSMutableArray arrayWithArray:_collapseView.subviews];
    NSMutableArray *arrayScrollView = [NSMutableArray array];
    
    for (UIView *subview in arraysubView) {
        if ([subview isKindOfClass:[UIScrollView class]]) {
            [arrayScrollView addObject:subview];
        }
    }
    
    NSLog(@"Size of ListPhoto: %lu",(unsigned long)self.listPhoto.count);
    
    for (NSUInteger i=0; i<self.listPhoto.count; i++) {

        if (i >= arrayScrollView.count) {
            break;
        }
        NSLog(@"i: %lu",(unsigned long)i);
        UIScrollView *scrollView = [arrayScrollView objectAtIndex:i];
            CGSize retinaSquare = scrollView.frame.size;
            
            [[PHImageManager defaultManager] requestImageForAsset:(PHAsset *)[self.listPhoto objectAtIndex:i]
                                                       targetSize:retinaSquare
                                                      contentMode:PHImageContentModeAspectFill
                                                          options:nil
                                                    resultHandler:^(UIImage *result, NSDictionary *info) {
                                                        // The result is not square, but correctly displays as a square using AspectFill
                                                        UIImageView *imgView = [[UIImageView alloc] initWithImage:result];
                                                        //            imgView.frame = CGRectMake(0, 0, scrollView.frame.size.width, scrollView.frame.size.height);
                                                        imgView.contentMode = UIViewContentModeScaleAspectFit;
                                                        [scrollView addSubview:imgView];
                                                        [scrollView setContentSize:imgView.frame.size];
                                                    }];
    }
}

#pragma mark - IB Action
- (IBAction)unWindFromShowSavedImage:(UIStoryboardSegue *)sender {
    
}
- (IBAction)pressCancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ShowSavedImage"]) {
        ShowSavedImageViewController *vc = segue.destinationViewController;
        vc.img = [_collapseView imageByRenderingView];
    }
}


@end
